<?php
/*
Plugin Name: Drissly Pay
Plugin URI: https://gitlab.com/luism23/payment-processor
Description: Plugin for Wordpress, used api Drissly Pay for generate checkout payment
Author: Luis Miguel Narvaez
Version: 1.0.3
Author URI: https://gitlab.com/luism23/
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/payment-processor',
	__FILE__,
	'DRISSLY-pay'
);
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
$myUpdateChecker->setBranch('master');


function DRISSLYPAY_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}

define("DRISSLYPAY_LOG",true);
define("DRISSLYPAY_PATH",plugin_dir_path(__FILE__));
define("DRISSLYPAY_URL",plugin_dir_url(__FILE__));

require_once DRISSLYPAY_PATH . "src/_index.php";