# DRISSLY Pay

Plugin for Wordpress, used api DRISSLY Pay for generate checkout payment

#### Version: 1.0.1

## Testing

#### Config

Test mode : true
![alt testingMode](https://gitlab.com/luism23/payment-processor/-/raw/master/doc/img/testingMode.png)


#### Checkout

```json
{
    "card_number" : 4004430000000007,
    "expiry_date" : "any",
    "cvv" : "any"
}
```

![alt testingCheckout](https://gitlab.com/luism23/payment-processor/-/raw/master/doc/img/testingCheckout.png)