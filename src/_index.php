<?php

require_once DRISSLYPAY_PATH . "src/img/_index.php";
require_once DRISSLYPAY_PATH . "src/function/_index.php";
require_once DRISSLYPAY_PATH . "src/css/_index.php";
require_once DRISSLYPAY_PATH . "src/js/_index.php";

require_once DRISSLYPAY_PATH . "src/api/_index.php";
require_once DRISSLYPAY_PATH . "src/hook/_index.php";
require_once DRISSLYPAY_PATH . "src/log/_index.php";
require_once DRISSLYPAY_PATH . "src/page/_index.php";
require_once DRISSLYPAY_PATH . "src/payment/_index.php";
require_once DRISSLYPAY_PATH . "src/routes/_index.php";
require_once DRISSLYPAY_PATH . "src/sass/_index.php";
require_once DRISSLYPAY_PATH . "src/template/_index.php";
require_once DRISSLYPAY_PATH . "src/validators/_index.php";