<?php


class API_DRISSLY_pay
{
    private $URL_PRO            = "https://acceso.drissly.com/api/v1";

    private $URL                = "";
    private $merchant_id        = "";
    private $password           = "";

    private $URL_DEV            = "https://sandbox.drissly.com/api/v1";
    private $email_DEV          = "zs10011598@gmail.com";
    private $password_DEV       = "Qwerty123";

    function __construct($atts = array())
    {
        $this->email  = $atts["email"];
        $this->password     = $atts["password"];
        $this->URL          = $this->URL_PRO;

        if($atts["testmode"]){
            $this->email        = $this->email_DEV;
            $this->password     = $this->password_DEV;
            $this->URL          = $this->URL_DEV;
        }
        $this->generateToken();
    }
    private function request($atts = array())
    {
        $curl = curl_init();
        $config =  array(
            CURLOPT_URL             => $this->URL.$atts["url"],
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => '',
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 0,
            CURLOPT_FOLLOWLOCATION  => 1,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => $atts["method"],
            CURLOPT_POSTFIELDS      => json_encode($atts["data"]),
            CURLOPT_HTTPHEADER      => array(
                'Content-Type: application/json',
                'Authorization: Bearer '.$this->TOKEN,
            ),
        );
        curl_setopt_array($curl,$config);

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response,true);

        $atts["header"] = array(                
            'Content-Type: application/json',
            'Authorization '.$this->TOKEN,
        );
        addDRISSLYPAY_LOG(array(
            "type"      => "REQUEST",
            "url"       => $this->URL.$atts["url"],
            "config"    => $atts,
            "result"    => $response,
        ));

        return $response;
    }
    private function generateToken()
    {
        $result = $this->request(array(
            "method"    => "POST",
            "url"       => "/auth/login",
            "data"      => array(
                "email" => $this->email,
                "password" => $this->password,
            )
        ));
        $this->TOKEN = $result["token"];
    }




    public function sale($atts = array())
    {
        $data = array(
            'phone'                 => $atts['phone'],
            'id_product'            => $atts['id_product'],
            'amount'                => $atts['amount'],
            'aditional'             => $atts['aditional'],
            'reference'             => $atts['order_id'],
        );
        addDRISSLYPAY_LOG(array(
            "type"      => "pre sale",
            "data"      => $data,
        ));
        return $this->request(array(
            "method"    => "POST",
            "url"       => "/purchase",
            "data"      => $data
        ));
    }
}