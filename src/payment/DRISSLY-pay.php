<?php
/*
 * This action hook registers our PHP class as a WooCommerce payment gateway
 */
add_filter( 'woocommerce_payment_gateways', 'DRISSLY_pay_add_gateway_class' );
function DRISSLY_pay_add_gateway_class( $gateways ) {
	$gateways[] = 'WC_DRISSLY_Pay_Gateway'; // your class name is here
	return $gateways;
}

/*
 * The class itself, please note that it is inside plugins_loaded action hook
 */
add_action( 'plugins_loaded', 'DRISSLY_pay_init_gateway_class' );
function DRISSLY_pay_init_gateway_class() {

	class WC_DRISSLY_Pay_Gateway extends WC_Payment_Gateway {

 		/**
 		 * Class constructor, more about it in Step 3
 		 */
 		public function __construct() {

            $this->id = 'drissly_pay'; // payment gateway plugin ID
            $this->icon = ''; // URL of the icon that will be displayed on checkout page near your gateway name
            $this->has_fields = true; // in case you need a custom credit card form
            $this->method_title = 'DRISSLY Pay Gateway';
            $this->method_description = 'Payment for Wordpress use DRISSLY Pay'; // will be displayed on the options page

            //$this->notification_url     = DRISSLYPAY_URL."src/routes/order/processing.php";
            //$this->txn_request_url      = get_home_url();
        
            // gateways can support subscriptions, refunds, saved payment methods,
            // but in this tutorial we begin with simple payments
            $this->supports = array(
                'products'
            );
        
            // Method with all the options fields
            $this->init_form_fields();
        
            // Load the settings.
            $this->init_settings();
            $this->title = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );
            $this->enabled = $this->get_option( 'enabled' );
            $this->testmode = 'yes' === $this->get_option( 'testmode' );
            //$this->txn_request_ip = $this->get_option( 'txn_request_ip' );
        
            // This action hook saves the settings
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

 		}
		/**
 		 * Plugin options, we deal with it in Step 3 too
 		 */
 		public function init_form_fields(){
            $this->form_fields = array(
                'enabled' => array(
                    'title'       => 'Enable/Disable',
                    'label'       => 'Enable DRISSLY Pay Gateway',
                    'type'        => 'checkbox',
                    'description' => '',
                    'default'     => 'no'
                ),
                'title' => array(
                    'title'       => 'Title',
                    'type'        => 'text',
                    'description' => 'This controls the title which the user sees during checkout.',
                    'default'     => 'DRISSLY Pay',
                    'desc_tip'    => true,
                ),
                'description' => array(
                    'title'       => 'Description',
                    'type'        => 'textarea',
                    'description' => 'This controls the description which the user sees during checkout.',
                    'default'     => 'Pay with your credit card via DRISSLY Pay',
                ),
                'configPayment' => array(
                    'title'       => 'Config Payment',
                    'type'        => 'tag',
                    'text'        => "Your config of payment, contact an advisor ",
                ),
                'testmode' => array(
                    'title'       => 'Test mode',
                    'label'       => 'Enable Test Mode',
                    'type'        => 'checkbox',
                    'description' => 'Place the payment gateway in test mode using test API keys.',
                    'default'     => 'yes',
                    'desc_tip'    => true,
                ),
                'email' => array(
                    'title'       => 'Email',
                    'type'        => 'text',
                ),
                'password' => array(
                    'title'       => 'Password',
                    'type'        => 'password',
                ),
            );
	 	}
         /**
          * Custon field tag
          */
        public function generate_tag_html( $key, $data ) { 
            $defaults = array(
                'class'             => '',
                'css'               => '',
                'custom_attributes' => array(),
                'desc_tip'          => false,
                'description'       => '',
                'title'             => '',
                'text'             => '',
            );
            $data = wp_parse_args( $data, $defaults );
            ob_start();
            ?>
            <tr valign="top" class="tag">
                <th scope="row" class="titledesc">
                    <label><?php echo wp_kses_post( $data['title'] ); ?> </label>
                </th>
                <td class="forminp">
                    <?php echo wp_kses_post( $data['text'] ); ?>
                </td>
            </tr>
            <style>
                .tag{
                    background: #1d2327;
                    color: white;
                    box-shadow: -50px 0 #1d2327,50px 0 #1d2327;
                }
                .tag label{
                    color: white !important;
                }
            </style>
            <?php
            return ob_get_clean();
        }
         /**
          * Custon field info
          */
        public function generate_info_html( $key, $data ) { 
            $defaults = array(
                'class'             => '',
                'css'               => '',
                'custom_attributes' => array(),
                'desc_tip'          => false,
                'description'       => '',
                'title'             => '',
                'text'             => '',
            );
            $data = wp_parse_args( $data, $defaults );
            ob_start();
            ?>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label><?php echo wp_kses_post( $data['title'] ); ?> </label>
                </th>
                <td class="forminp">
                    <?php echo wp_kses_post( $data['text'] ); ?>
                </td>
            </tr>
            <?php
            return ob_get_clean();
        }

		/**
		 * You will need it if you want your custom credit card form, Step 4 is about it
		 */
		public function payment_fields() {
                // ok, let's display some description before the payment form
                if ( $this->description ) {
                    // you can instructions for test mode, I mean test card numbers etc.
                    if ( $this->testmode ) {
                        $this->description = ' TEST MODE ENABLED.';
                    }
                    // display the description with <p> tags etc.
                    echo wpautop( wp_kses_post( $this->description ) );
                }
                ?>
                <div id="DRISSLYPAY_content_cc" class="DRISSLYPAY_content active">
                    <div class="form-row form-row-wide " >
                        <label>
                            Card Number <span class="required">*</span>
                        </label>
                        <div class="DRISSLYPAY_content_card_number">
                            <input id="DRISSLYPAY_card_number" name="DRISSLYPAY_card_number" type="text" autocomplete="off" required placeholder="Card Number">
                            <img src="" alt="" id="DRISSLYPAY_card_number_img" class="DRISSLYPAY_card_number_img">
                        </div>
                    </div>
                    <div class="form-row form-row-wide pos-r">
                        <label>
                            Expiry Date (YYYY-MM) <span class="required">*</span>
                        </label>
                        <input id="DRISSLYPAY_expiry_date" name="DRISSLYPAY_expiry_date" type="text" autocomplete="off" required placeholder="YYYY-MM">
                        <input id="DRISSLYPAY_expiry_date_hidden" name="DRISSLYPAY_expiry_date_hidden" type="month" autocomplete="off" required class="inputDateOver">
                    </div>
                    <div class="form-row form-row-wide">
                        <label>
                            CVV <span class="required">*</span>
                        </label>
                        <input id="DRISSLYPAY_cvv" name="DRISSLYPAY_cvv" type="number" autocomplete="off" required max="999" placeholder="XXX" maxlength="3">
                    </div>
                    <div class="clear"></div>
                </div>
                <script>
                    var DRISSLYPAY_LOG = <?=DRISSLYPAY_LOG ? "true":"false"?> ;
                    var DRISSLYPAY_URL = "<?=DRISSLYPAY_URL?>";
                </script>
                <link rel="stylesheet" href="<?=DRISSLYPAY_URL?>src/css/DRISSLY-pay-checkout.css?v=<?=DRISSLYPAY_get_version()?>">
                <script src="<?=DRISSLYPAY_URL?>src/js/DRISSLY-pay-checkout.js?v=<?=DRISSLYPAY_get_version()?>"></script>
                <?php
		}

		/*
 		 * Fields validation, more in Step 5
		 */
		public function validate_fields() {
            if(!$this->testmode){
                if( empty( $this->get_option( 'email' ) )) {
                    wc_add_notice(  'DRISSLY Pay Email is required!', 'error' );
                    return false;
                }
                if( empty( $this->get_option( 'password' ) )) {
                    wc_add_notice(  'DRISSLY Pay Password is required!', 'error' );
                    return false;
                }
            }
            if( empty( $_POST['DRISSLYPAY_card_number'] )) {
                wc_add_notice(  'Card Number is required!', 'error' );
                return false;
            }
            if( empty( $_POST['DRISSLYPAY_cvv'] )) {
                wc_add_notice(  'CVV is required!', 'error' );
                return false;
            }
            if( empty( $_POST['DRISSLYPAY_expiry_date'] )) {
                wc_add_notice(  'Expiry Date is required!', 'error' );
                return false;
            }
            if( empty( $_POST['billing_phone'] )) {
                wc_add_notice(  'Phone is required!', 'error' );
                return false;
            }
            if(!preg_match("/[+][0-9]{1,3}[-][0-9]{10}/", $_POST['billing_phone'])){
                wc_add_notice(  'Phone is Invalid!, the format of Phone is: "Code + Number(10 digit)"', 'error' );
                return false;
            }
            return true;
		}
        public function getApi()
        {
            $api = new API_DRISSLY_pay(array(
                "email"         =>  $this->get_option( 'email' ),
                "password"      =>  $this->get_option( 'password' ),
                "testmode"      =>  $this->testmode,
            ));
            return $api;
        }
		/*
		 * We're processing the payments here, everything about it is in Step 5
		 */
		public function process_payment( $order_id ) {
            try {
                global $woocommerce;
    
                $order = wc_get_order( $order_id );
    
                $api = $this->getApi();

                
                foreach ( $order->get_items() as $item_id => $item ) {
                    $product_id = $item->get_product_id();
                }

                $result = $api->sale(array(
                    'phone'          => $order->get_billing_phone(),
                    'id_product'            => $product_id,
                    'amount'                => floatval($order->get_total()),
                    'aditional'=>'',
                    'order_id'              => $order->get_id(),
                ));
               
                if($result["status_code"] == 0){

                    $order->payment_complete();
                    $order->reduce_order_stock();
                    $order->add_order_note( 'Hey, your order is paid! Thank you!', true );
                    $woocommerce->cart->empty_cart();
        
                    update_post_meta(
                        $order_id,
                        "payment",
                        "DRISSLY_pay"
                    );
                    update_post_meta(
                        $order_id,
                        "payment_DRISSLY_pay",
                        json_encode($result)
                    );
                    addDRISSLYPAY_LOG(array(
                        "type"      => "sale-success",
                        "data"     => $result,
                    ));
                    return array(
                        'result' => 'success',
                        'redirect' => $order->get_checkout_order_received_url()
                    );	
                }

                addDRISSLYPAY_LOG(array(
                    "type"      => "sale-error",
                    "error"     => $result,
                ));
                wc_add_notice( $result["message"], 'error' );
                return false;

            } catch (Exception $error) {
                addDRISSLYPAY_LOG(array(
                    "type"      => "sale-error",
                    "error"     => $error,
                ));
                wc_add_notice(  $error->getMessage(), 'error' );
                return false;
            }
	 	}
 	}
}