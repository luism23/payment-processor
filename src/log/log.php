<?php
if(DRISSLYPAY_LOG){
    function add_DRISSLYPAY_LOG_option_page($admin_bar)
    {
        global $pagenow;
        $admin_bar->add_menu(
            array(
                'id' => 'DRISSLYPAY_LOG',
                'title' => 'DRISSLYPAY_LOG',
                'href' => get_site_url().'/wp-admin/options-general.php?page=DRISSLYPAY_LOG'
            )
        );
    }

    function DRISSLYPAY_LOG_option_page()
    {
        add_options_page(
            'Log DRISSLY PAYS',
            'Log DRISSLY PAYS',
            'manage_options',
            'DRISSLYPAY_LOG',
            'DRISSLYPAY_LOG_page'
        );
    }

    function DRISSLYPAY_LOG_page()
    {
        $log = DRISSLYPAY_get_optionPage("DRISSLYPAY_LOG");
        $log = array_reverse($log);
        ?>
        <script>
            const log = <?=json_encode($log)?>;
        </script>
        <h1>
            Log
        </h1>
        <pre><?php var_dump($log);?></pre>
        <?php
    }
    add_action('admin_bar_menu', 'add_DRISSLYPAY_LOG_option_page', 100);

    add_action('admin_menu', 'DRISSLYPAY_LOG_option_page');

}

function addDRISSLYPAY_LOG($newLog)
{
    if(!DRISSLYPAY_LOG){
        return;
    }
    DRISSLYPAY_put_optionPage("DRISSLYPAY_LOG",$newLog);
}