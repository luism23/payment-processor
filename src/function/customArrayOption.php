<?php

function DRISSLYPAY_get_optionPage($id = "DRISSLYPAY_optionPage")
{
    $DRISSLYPAY_optionPage = get_option($id);
    if($DRISSLYPAY_optionPage === false || $DRISSLYPAY_optionPage == null || $DRISSLYPAY_optionPage == ""){
        $DRISSLYPAY_optionPage = "[]";
    }
    $DRISSLYPAY_optionPage = json_decode($DRISSLYPAY_optionPage,true);
    return $DRISSLYPAY_optionPage;
}

function DRISSLYPAY_put_optionPage($id,$newItem)
{
    $DRISSLYPAY_optionPage = DRISSLYPAY_get_optionPage($id);
    $DRISSLYPAY_optionPage[] = array(
        "date" => date("c"),
        "data" => $newItem,
    );
    update_option($id,json_encode($DRISSLYPAY_optionPage));
}